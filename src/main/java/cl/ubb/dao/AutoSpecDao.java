package cl.ubb.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.AutoSpec;
import cl.ubb.model.AutoTipo;

public interface AutoSpecDao extends CrudRepository<AutoSpec, Long>{
	
	@Query("select as.marca as.modelo as.anio from AutoSpec"
			+ "where as.id = ?1")
	public ArrayList<AutoSpec> findMarcaModeloAnio();
	

}
