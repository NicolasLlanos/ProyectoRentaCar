package cl.ubb.model;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class Sucursal {
	 @Id
		@GeneratedValue
	    
		private long id;
		private String ciudad;
		private String sector;
		
		public Sucursal(){
			
		}
		
		public String getCiudad(){
			return ciudad;
		}
		
		public void setCiudad(String ciudad){
			this.ciudad = ciudad;
		}
		
		public String getSector(){
			return  sector;
		}
		
		public void setSector(String sector){
			this.sector = sector;
		}
		
		public long getId(){
			return id;
		}
		
		public void setId(long id){
			this.id = id;
		}

}
