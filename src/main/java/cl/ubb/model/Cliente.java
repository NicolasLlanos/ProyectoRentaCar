package cl.ubb.model;

import javax.persistence.Entity;

@Entity
public class Cliente {
	 
     private String Rut;
     private String Nombre;
     private String Email;
     private int NumeroCelular;
     
     public Cliente(){
    	 
     }
     
     public String getRut(){
    	 return Rut;
     }
     
     public void setRut(String Rut){
 		this.Rut = Rut;
 	}
 	
 	public String getNombre(){
 		return  Nombre;
 	}
 	
 	public void setNombre(String Nombre){
 		this.Nombre = Nombre;
 	}
 	
 	public void setEmail(String Email){
 		this.Email = Email;
 	}
 	
 	public int getNumeroCelular(){
 		return NumeroCelular;
 	}
 	
 
 	public void setNumeroCelular(int NumeroCelular){
 		this.NumeroCelular = NumeroCelular;
 	}
     
}
