package cl.ubb.model;

import javax.persistence.Entity;

@Entity
public class Auto {

	private String Patente;
    private String Color;
	
    public Auto(){	
    	  }
    public String getPatente(){
    	return Patente;
    }
    public void setPatente(String Patente){
    	this.Patente = Patente;
    }
    public String getColor(){
    	return Color;
    }
    public void setColor(String Color){
    	this.Color = Color;
    }
    
}
