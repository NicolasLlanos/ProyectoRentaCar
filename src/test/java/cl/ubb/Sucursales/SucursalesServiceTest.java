package cl.ubb.Sucursales;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.ClientesDao;
import cl.ubb.dao.SucursalesDao;
import cl.ubb.model.Sucursal;
import cl.ubb.service.ClientesService;
import cl.ubb.service.SucursalesService;
@RunWith(MockitoJUnitRunner.class)
public class SucursalesServiceTest {
	@Mock
	private SucursalesDao SucursalesDao;
	
	@InjectMocks
	private SucursalesService SucursalesService;

	@Test
	public void deboListarSucursales(){
		//arrange 
		List<Sucursal> misSucursales = new ArrayList<Sucursal>();
		//act
		when(SucursalesDao.findAll()).thenReturn(misSucursales);
		ArrayList<Sucursal> resultado = SucursalesService.obtenerTodos();
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(misSucursales, resultado);
	}
}
