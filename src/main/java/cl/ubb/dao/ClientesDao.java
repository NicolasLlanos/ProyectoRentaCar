package cl.ubb.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Cliente;


public interface ClientesDao extends CrudRepository<Cliente, String>{
	@Query("select C.Rut C.Nombre from Cliente C ")
	public ArrayList<Cliente> findByRutNombre();
	
	
	public boolean findByRut(String string);
}


	
	
	

