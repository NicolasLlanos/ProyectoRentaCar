package cl.ubb.model;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


public class AutoTipo {

	@Id
	@GeneratedValue
	
	private long id;
	private String nombre;
	private String transmision;
	private String combustible;
	private boolean aireacondicionado;
	private int airbag;
	private int pasajeros;
	private int preciodiario;
	
	public long getId (){
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	public String getNombre (){
		return nombre;
	}
	
	public void setNombre(String nombre){
		this.nombre = nombre ;
	}
	public String getTransmision(){
		return transmision;
	}
	
	public void setTransmision(String transmision){
		this.transmision = transmision ;
		
	}
	public Boolean getAireacondicionado (){
		return aireacondicionado;
	}
	
	public void setAireacondicionado(Boolean aireacondicionado){
		this.aireacondicionado = aireacondicionado ;
	}
	public int getAirbag(){
		return airbag;
	}
	
	public void setAirbag(int airbag){
		this.airbag = airbag ;
	}
	public int getPasajeros(){
		return pasajeros;
	}
	
	public void setPasajeros(int pasajeros){
		this.pasajeros = pasajeros;
	}
	public int getPreciodiario(){
		return preciodiario;
	}
	
	public void setPreciodiario(int preciodiario){
		this.preciodiario = preciodiario;
	}
	
	
}
