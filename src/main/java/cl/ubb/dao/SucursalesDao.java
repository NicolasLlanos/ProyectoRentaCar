package cl.ubb.dao;

import java.util.ArrayList;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Sucursal;

public interface SucursalesDao extends CrudRepository<Sucursal, Long>{

@Query("select s.id s.ciudad from Sucursal s")
public ArrayList<Sucursal> findByidsucursal();	
	
	
}
