package cl.ubb.model;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;


public class Booking {
	@Id
	@GeneratedValue    
	private long id;
	
	private Date fechaInicio;
	private Date fechaTermino;
	private int montoDeuda;
	private String rutCliente;
	private long idAuto;
	private String categoria;
	
	
	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRutCliente() {
		return rutCliente;
	}

	public void setRutCliente(String rutCliente) {
		this.rutCliente = rutCliente;
	}

	public long getIdAuto() {
		return idAuto;
	}

	public void setIdAuto(long idAuto) {
		this.idAuto = idAuto;
	}

	public Date getFechaInicio (){
		return fechaInicio;
	}
	
	public void setFechaInicio(Date fechaInicio){
		this.fechaInicio = fechaInicio ;
	}
	public Date getFechaTermino(){
		return fechaTermino;
	}
	
	public void setFechaTermino(Date fechaTermino){
		this.fechaTermino = fechaTermino ;
		
	}
	
	public int getMontoDeuda(){
		return montoDeuda;
	}
	
	public void setMontoDeuda(int montoDeuda){
		this.montoDeuda = montoDeuda;
	}
	
	
}
