package cl.ubb.model;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class AutoSpec {
	@Id
	@GeneratedValue
	
	private long id;
	private String marca;
	private String modelo;
	private int anio;
	
	
	public long getId (){
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	public String getMarca (){
		return marca;
	}
	
	public void setMarca(String marca){
		this.marca = marca;
	}
	public String getModelo(){
		return modelo;
	}
	
	public void setModelo(String modelo){
		this.modelo = modelo ;
		
	}
	public int getAnio (){
		return anio;
	}
	
	public void setAnio(int anio){
		this.anio = anio ;
	}
	
}
