package cl.ubb.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


import cl.ubb.model.Booking;

public interface BookingDao extends CrudRepository<Booking, Long>{

	@Query("select b.idAuto b.fechaInicio b.fechaTermino from Booking b where b.fechaInicio>= ?1")
	ArrayList<Booking> obtenerPorFecha(Booking booking);

}
