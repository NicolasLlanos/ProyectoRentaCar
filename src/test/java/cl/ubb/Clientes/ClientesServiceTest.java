package cl.ubb.Clientes;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.model.Cliente;
import cl.ubb.dao.ClientesDao;
import cl.ubb.service.ClientesService;


import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;

@RunWith(MockitoJUnitRunner.class)

public class ClientesServiceTest {
	@Mock
	private ClientesDao ClientesDao;
	
	@InjectMocks
	private ClientesService ClientesService;
	
	@Test
	public void deboRegistrarCliente(){
		//arrange 
		Cliente cliente = new Cliente();
		cliente.setNombre("Nicolas");
		cliente.setRut("19087734-5");
		cliente.setEmail("nicolas.wachitorico@gmail.com");
		cliente.setNumeroCelular(99328423);
		
		//act
		when(ClientesDao.save(cliente)).thenReturn(cliente);
		Cliente clienteRegistrado = ClientesService.registrarCliente(cliente);
		
		//assert
				assertNotNull(clienteRegistrado);
				assertEquals(clienteRegistrado,cliente);
	}
	
	@Test
	public void deboListarCaterogiaClientes(){
		//arrange 
		List<Cliente> misClientes = new ArrayList<Cliente>();
		//act
		when(ClientesDao.findAll()).thenReturn(misClientes);
		ArrayList<Cliente> resultado = ClientesService.obtenerTodos();
		
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(misClientes, resultado);
	}
	
	@Test
	public void consultaClienteRegistrado(){
		//arrange
		Cliente cliente = new Cliente();
		boolean clienteRegistrado;
		cliente.setRut("17745574-1");
		//act
		when(ClientesDao.exists(cliente.getRut())).thenReturn(true);
		//when(ClientesDao.findByRut("17745574-1")).thenReturn(cliente);
		clienteRegistrado = ClientesService.findByRut(cliente.getRut());
		
		//assert 
		Assert.assertNotNull(clienteRegistrado);
		Assert.assertEquals(true,clienteRegistrado);
		
		}
	
	
}
