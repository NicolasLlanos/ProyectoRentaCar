package cl.ubb.Autos;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.when;

import cl.ubb.dao.AutoSpecDao;
import cl.ubb.model.AutoSpec;
import cl.ubb.service.AutoSpecService;

@RunWith(MockitoJUnitRunner.class)

public class AutoSpecServiceTest {

	@Mock
	private AutoSpecDao AutoSpecDao;
	
	@InjectMocks
	private AutoSpecService AutoSpecService;
	
	@Test
	public void deboListarAutosPorTipo(){
		//arrange
		List<AutoSpec> misAutoSpecs = new ArrayList<AutoSpec>();
		
		//act
		when(AutoSpecDao.findAll()).thenReturn(misAutoSpecs);
		ArrayList<AutoSpec>  resultado = AutoSpecService.obtenerPorId(1L);
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(misAutoSpecs, resultado);
	
		
	}
	
}
