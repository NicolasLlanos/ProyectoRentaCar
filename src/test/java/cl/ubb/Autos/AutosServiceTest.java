package cl.ubb.Autos;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
//import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.when;

import cl.ubb.dao.AutosDao;

import cl.ubb.model.Auto;
import cl.ubb.model.AutoTipo;

import cl.ubb.service.AutosService;

@RunWith(MockitoJUnitRunner.class)
public class AutosServiceTest {
	
	@Mock
	private AutosDao autosDao;
	
	
	@InjectMocks
	private AutosService AutosService;
	
	@Test
	public void deboListarTipoAuto(){
		//arrange
		List<AutoTipo> misAutoTipos = new ArrayList<AutoTipo>();
		
		//act
	    when(autosDao.findAll()).thenReturn(misAutoTipos);
	    ArrayList<AutoTipo> resultado = AutosService.obtenerPorCategoria("lujo");
	    
	    //assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(misAutoTipos,resultado);
		
		
	}

}
