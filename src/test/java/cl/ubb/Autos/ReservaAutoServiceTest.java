package cl.ubb.Autos;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;


import cl.ubb.dao.AutosDao;
import cl.ubb.dao.BookingDao;
import cl.ubb.dao.ClientesDao;
import cl.ubb.model.AutoSpec;
import cl.ubb.model.AutoTipo;
import cl.ubb.model.Booking;
import cl.ubb.model.Cliente;
import cl.ubb.service.BookingService;

@RunWith(MockitoJUnitRunner.class)

public class ReservaAutoServiceTest {
	@Mock
	private BookingDao bookingDao;

	
	@InjectMocks
	private BookingService BookingService;
	
	@Test
	public void deboReservarAuto(){
		//arrange
		Booking booking = new Booking();
		Date diaInicio = new Date(2017,04,23);
		booking.setFechaInicio(diaInicio);
		Date diaFin = new Date(2017,05,23);
		booking.setFechaTermino(diaFin);
		booking.setCategoria("lujo");
		booking.setIdAuto(234234123);
		booking.setRutCliente("17745574-1");
		
		//act
		when(bookingDao.save(booking)).thenReturn(booking);
		Booking bookingCreado = BookingService.registrarBooking(booking);
		//assert
		assertNotNull(bookingCreado);
		assertEquals(bookingCreado,booking);
		
	}

	
	@Test
	public void deboListarReservasClienteDesdeFecha(){
		Booking booking = new Booking();
		Date diaInicio = new Date(2017,04,23);
		booking.setFechaInicio(diaInicio);
		booking.setRutCliente("17745574-1");
		//Date diaFin = new Date(2017,05,23);
		//arrange
		List<Booking> misBooking = new ArrayList<Booking>();
		
		//act
		when(bookingDao.findAll()).thenReturn(misBooking);
		ArrayList<Booking>  resultado = BookingService.obtenerPorFecha(booking);
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(misBooking, resultado);
	
		
	}
	
	
}
