package cl.ubb.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.BookingDao;
import cl.ubb.dao.ClientesDao;
import cl.ubb.model.Booking;
import cl.ubb.model.Cliente;

public class BookingService {

	private Booking booking;
	private BookingDao bookingDao;
	@Autowired
	
	public Booking registrarBooking(Booking booking){
		return bookingDao.save(booking);
	}
	
	
	public ArrayList<Booking> obtenerPorFecha(Booking booking) {
		return bookingDao.obtenerPorFecha(booking);
		
	}
}
