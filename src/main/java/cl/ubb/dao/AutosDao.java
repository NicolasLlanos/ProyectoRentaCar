

///Este deberia ser AutoTipoDao

package cl.ubb.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.AutoTipo;


public interface AutosDao extends CrudRepository<AutoTipo, String>{

	@Query("select a.id a.nombre a.transmision a.combustible "
			+ "a.pasajeros from AutoTipo a where a.nombre = ?1")
	public ArrayList<AutoTipo> findCasiAll();	
		
}
